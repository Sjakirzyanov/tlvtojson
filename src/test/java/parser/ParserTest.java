package parser;

import exception.ByteConverterException;
import org.junit.Test;
import util.TestUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class ParserTest {

    @Test
    public void testNext(){
        String dataString = "01000400A8329256";
        byte data[] = TestUtils.convertStringToByteArray(dataString);
        Parser parser = new Parser(data);
        ParsedField field = parser.next();
        assertEquals(field, new ParsedField(1, TestUtils.convertStringToByteArray("A8329256")));
    }

    @Test(expected = ByteConverterException.class)
    public void testNextException(){
        String dataSring[] = {"", "8329", "01"};
        for (int i= 0; i<dataSring.length; i++){
            byte data[] = TestUtils.convertStringToByteArray(dataSring[i]);
            Parser parser = new Parser(data);
            parser.next();
        }
    }

    @Test
    public void testToFirstField(){
        String dataString = "01000400A8329256";
        byte data[] = TestUtils.convertStringToByteArray(dataString);
        Parser parser = new Parser(data);
        ParsedField field1 = parser.next();
        parser.toFirstField();
        ParsedField field2 = parser.next();
        assertEquals(field1, field2);
    }

    @Test
    public void testHasNext(){
        String dataSring[] = {"", "8329", "01000400A8329256"};
        boolean expected[] = {false, false, true};
        for (int i = 0; i<dataSring.length; i++){
            Parser parser = new Parser(TestUtils.convertStringToByteArray(dataSring[i]));
            assertEquals(expected[i], parser.hasNext());
        }
    }

    @Test
    public void testAll(){
        String dataString = "01000400A832925602000300047102";
        byte data[] = TestUtils.convertStringToByteArray(dataString);
        List<ParsedField> expected = Arrays.asList(
                new ParsedField(1, TestUtils.convertStringToByteArray("A8329256")),
                new ParsedField(2, TestUtils.convertStringToByteArray("047102"))
        );
        List<ParsedField> actual = new ArrayList<>();
        Parser parser = new Parser(data);
        while (parser.hasNext()){
            actual.add(parser.next());
        }
        assertEquals(expected, actual);
    }
}
