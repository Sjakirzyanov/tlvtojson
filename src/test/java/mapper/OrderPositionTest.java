package mapper;


import exception.UnsupportedTagException;
import model.OrderPosition;
import org.junit.Test;
import util.TestUtils;

import java.math.BigDecimal;
import java.math.BigInteger;

import static org.junit.Assert.assertEquals;

public class OrderPositionTest {

    @Test(expected = UnsupportedTagException.class)
    public void testException(){
        String stringData = "0B00070084EBE0AEAAAEAB1C000200204E0D00020000020E000200409C";
        byte[] data = TestUtils.convertStringToByteArray(stringData);
        OrderPositionMapper mapper = new OrderPositionMapper(data);
        mapper.map();
    }

    @Test
    public void testMap(){
        String stringData = "0B00070084EBE0AEAAAEAB0C000200204E0D00020000020E000200409C";
        byte[] data = TestUtils.convertStringToByteArray(stringData);
        OrderPositionMapper mapper = new OrderPositionMapper(data);
        OrderPosition actual = mapper.map();
        assertEquals("Дырокол", actual.getName());
        assertEquals(new BigInteger("20000"), actual.getPrice());
        assertEquals(new BigInteger("40000"), actual.getSum());
        assertEquals(new BigDecimal("2"), actual.getQuantity());
    }
}
