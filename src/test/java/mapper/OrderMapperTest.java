package mapper;


import exception.UnsupportedTagException;
import model.Order;
import model.OrderPosition;
import org.junit.Test;
import util.TestUtils;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.Date;

import static org.junit.Assert.assertEquals;

public class OrderMapperTest {

    @Test(expected = UnsupportedTagException.class)
    public void testException(){
        String stringData = "09000400A83292560200030004710203000B008E8E8E209" +
                "0AEACA0E8AAA004001D000B00070084EBE0AEAAAEAB0C000200204E0D00020000020E000200409C";
        byte[] data = TestUtils.convertStringToByteArray(stringData);
        OrderMapper mapper = new OrderMapper(data);
        mapper.map();
    }

    @Test
    public void testMap(){
        String stringData = "01000400A83292560200030004710203000B008E8E8E209" +
                "0AEACA0E8AAA004001D000B00070084EBE0AEAAAEAB0C000200204E0D00020000020E000200409C";
        byte[] data = TestUtils.convertStringToByteArray(stringData);
        OrderMapper mapper = new OrderMapper(data);
        Order actual = mapper.map();
        assertEquals(actual.getCustomerName(), "ООО Ромашка");
        assertEquals(actual.getDateTime(), new Date(1452421800L*1000));
        assertEquals(actual.getOrderNumber(), new BigInteger("160004"));
        assertEquals(actual.getItems(), Arrays.asList(
                new OrderPosition("Дырокол", new BigInteger("20000"),new BigDecimal("2"), new BigInteger("40000"))
        ));
    }
}
