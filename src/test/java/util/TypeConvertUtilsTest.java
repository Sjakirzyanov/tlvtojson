package util;


import org.junit.Test;
import util.TypeConvertUtils;

import java.math.BigDecimal;
import java.math.BigInteger;

import static org.junit.Assert.assertEquals;

public class TypeConvertUtilsTest {

    @Test
    public void convertUInt32Test() {
        String stringBytes[] = {
                "01000400", "EBE0AEAA", "00020E00", "A8329256", "FFFFFFFF"
        };
        long[] results = {262145, 2863587563L, 918016, 1452421800L, 4294967295L};
        for (int i = 0; i < results.length; i++) {
            byte[] bytes = TestUtils.convertStringToByteArray(stringBytes[i]);
            assertEquals(TypeConvertUtils.convertUInt32(bytes), results[i]);
        }
    }

    @Test
    public void convertVLNTest() {
        String stringBytes[] = {
                "01000400",
                "EBE0AEAA",
                "00020E00",
                "A8329256",
                "FFFFFFFF",
                "FFFFFFFFFFFF",
                "710400030002569232"
        };
        BigInteger[] results = {
                new BigInteger("262145"), new BigInteger("2863587563"),
                new BigInteger("918016"), new BigInteger("1452421800"),
                new BigInteger("4294967295"), new BigInteger("281474976710655"),
                new BigInteger("932881821462085764209")
        };
        for (int i = 0; i < results.length; i++) {
            byte[] bytes = TestUtils.convertStringToByteArray(stringBytes[i]);
            assertEquals(results[i], TypeConvertUtils.convertVLN(bytes));
        }
    }

    @Test
    public void convertFVLNTest() {
        String[] stringBytes = {
                "0001000400",
                "04EBE0AEAA",
                "0200020E00",
                "03A8329256",
                "0CFFFFFFFF",
                "04FFFFFFFFFFFF",
                "06710400030002569232"
        };
        BigDecimal[] results = {new BigDecimal("262145"), new BigDecimal("286358.7563"),
                new BigDecimal("9180.16"), new BigDecimal("1452421.800"),
                new BigDecimal("0.004294967295"), new BigDecimal("28147497671.0655"),
                new BigDecimal("932881821462085.764209")};
        for (int i = 0; i < results.length; i++) {
            assertEquals(results[i], TypeConvertUtils.convertFVLN(TestUtils.convertStringToByteArray(stringBytes[i])));
        }
    }

    @Test
    public void convertStringTest() {
        String stringBytes[] = {
                "8E8E8E2090AEACA0E8AAA0",
                "84EBE0AEAAAEAB"
        };
        String[] results = {"ООО Ромашка","Дырокол"};
        for (int i = 0; i < results.length; i++) {
            byte[] bytes = TestUtils.convertStringToByteArray(stringBytes[i]);
            assertEquals(TypeConvertUtils.convertString(bytes), results[i]);
        }
    }
}
