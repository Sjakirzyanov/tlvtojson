package parser;

import exception.ByteConverterException;
import util.TypeConvertUtils;

public class Parser {

    private byte data[];
    private int currByteInd;

    private static final int LENGTH_FIELD_BYTE_COUNT = 2;
    private static final int TAG_FIELD_BYTE_COUNT = 2;

    public Parser(byte[] data) {
        this.data = data;
    }

    public boolean hasNext() {
        return currByteInd + TAG_FIELD_BYTE_COUNT + LENGTH_FIELD_BYTE_COUNT < data.length - 1;
    }

    public ParsedField next() {
        if(data.length <= TAG_FIELD_BYTE_COUNT + LENGTH_FIELD_BYTE_COUNT){
            throw new ByteConverterException("Can't convert data. Data length is less than"+
                    (TAG_FIELD_BYTE_COUNT + LENGTH_FIELD_BYTE_COUNT));
        }
        byte tagBytes[] = new byte[TAG_FIELD_BYTE_COUNT];
        byte lengthBytes[] = new byte[LENGTH_FIELD_BYTE_COUNT];

        System.arraycopy(data, currByteInd, tagBytes, 0, TAG_FIELD_BYTE_COUNT);
        System.arraycopy(data, currByteInd + TAG_FIELD_BYTE_COUNT, lengthBytes, 0, LENGTH_FIELD_BYTE_COUNT);
        int tag = TypeConvertUtils.convertVLN(tagBytes).intValue();
        int length = TypeConvertUtils.convertVLN(lengthBytes).intValue();

        byte fieldData[] = new byte[length];
        currByteInd += TAG_FIELD_BYTE_COUNT + LENGTH_FIELD_BYTE_COUNT;
        System.arraycopy(data, currByteInd, fieldData, 0, length);
        currByteInd += length;
        return new ParsedField(tag, fieldData);
    }

    public void toFirstField() {
        currByteInd = 0;
    }
}
