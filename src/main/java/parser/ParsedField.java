package parser;

import java.util.Arrays;

/**
 * Created by arsen on 06.05.17.
 */
public class ParsedField {
    private int tag;
    private byte [] data;

    public ParsedField(int tag, byte[] data) {
        this.tag = tag;
        this.data = data;
    }

    public int getTag() {
        return tag;
    }

    public void setTag(int tag) {
        this.tag = tag;
    }

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ParsedField that = (ParsedField) o;

        if (tag != that.tag) return false;
        return Arrays.equals(data, that.data);
    }

    @Override
    public int hashCode() {
        int result = tag;
        result = 31 * result + Arrays.hashCode(data);
        return result;
    }

    @Override
    public String toString() {
        return "ParsedField{" +
                "tag=" + tag +
                ", data=" + Arrays.toString(data) +
                '}';
    }
}
