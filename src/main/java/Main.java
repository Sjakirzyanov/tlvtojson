import com.fasterxml.jackson.databind.ObjectMapper;
import mapper.OrderMapper;
import model.Order;
import org.apache.commons.io.FileUtils;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;


public class Main {
    public static void main(String args[]){
        if(args.length < 2)
            System.out.println("Wrong argument count");
        try {
            byte[] sourceFileContent = FileUtils.readFileToByteArray(new File(args[0]));
            OrderMapper mapper = new OrderMapper(sourceFileContent);
            Order order = mapper.map();
            ObjectMapper jsonMapper = new ObjectMapper();
            Path outFilePath = Paths.get(args[1]);
            List<String> fileContent = Arrays.asList(jsonMapper.writeValueAsString(order));
            Files.write(outFilePath, fileContent, StandardCharsets.UTF_8);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
