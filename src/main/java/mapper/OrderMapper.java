package mapper;


import exception.UnsupportedTagException;
import model.Order;
import model.OrderPosition;
import parser.ParsedField;
import parser.Parser;
import util.TypeConvertUtils;

import java.math.BigInteger;
import java.util.Date;

public class OrderMapper extends ObjectMapper<Order>{

    public OrderMapper(byte[] data) {
        super(data);
    }

    @Override
    public Order map() {
        Parser parser = new Parser(data);
        Order order = new Order();
        while (parser.hasNext()){
            ParsedField field = parser.next();
            switch (field.getTag()){
                case 1:
                    long unixTime = TypeConvertUtils.convertUInt32(field.getData());
                    order.setDateTime(new Date(unixTime*1000));
                    break;
                case 2:
                    BigInteger orderNum = TypeConvertUtils.convertVLN(field.getData());
                    order.setOrderNumber(orderNum);
                    break;
                case 3:
                    String customerName = TypeConvertUtils.convertString(field.getData());
                    order.setCustomerName(customerName);
                    break;
                case 4:
                    OrderPositionMapper orderPosMapper = new OrderPositionMapper(field.getData());
                    OrderPosition orderPos = orderPosMapper.map();
                    order.getItems().add(orderPos);
                    break;
                default:
                    throw new UnsupportedTagException("Can't recognize tag: "+field.getTag());
            }
        }
        return order;
    }
}
