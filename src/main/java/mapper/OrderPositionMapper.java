package mapper;

import exception.UnsupportedTagException;
import model.OrderPosition;
import parser.ParsedField;
import parser.Parser;
import util.TypeConvertUtils;

import java.math.BigDecimal;
import java.math.BigInteger;


public class OrderPositionMapper extends ObjectMapper<OrderPosition>{

    public OrderPositionMapper(byte[] data) {
        super(data);
    }

    @Override
    public OrderPosition map() {
        Parser parser = new Parser(data);
        OrderPosition pos = new OrderPosition();
        while (parser.hasNext()){
            ParsedField field = parser.next();
            switch (field.getTag()){
                case 11:
                    String orderPosName = TypeConvertUtils.convertString(field.getData());
                    pos.setName(orderPosName);
                    break;
                case 12:
                    BigInteger price = TypeConvertUtils.convertVLN(field.getData());
                    pos.setPrice(price);
                    break;
                case 13:
                    BigDecimal quantity = TypeConvertUtils.convertFVLN(field.getData());
                    pos.setQuantity(quantity);
                    break;
                case 14:
                    BigInteger sum = TypeConvertUtils.convertVLN(field.getData());
                    pos.setSum(sum);
                    break;
                default:
                    throw new UnsupportedTagException("Can't recognize tag: "+field.getTag());
            }
        }
        return pos;
    }
}
