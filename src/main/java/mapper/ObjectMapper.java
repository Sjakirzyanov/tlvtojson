package mapper;


import model.Mappable;

public abstract class ObjectMapper<T extends Mappable> {
    protected byte data[];
    protected static final int LENGTH_FIELD_BYTE_COUNT = 2;
    protected static final int TAG_FIELD_BYTE_COUNT = 2;

    public ObjectMapper(byte[] data) {
        this.data = data;
    }

    public abstract T map();

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }
}
