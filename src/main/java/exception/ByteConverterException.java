package exception;


public class ByteConverterException extends RuntimeException {
    public ByteConverterException(String message) {
        super(message);
    }
}
