package exception;


public class UnsupportedTagException extends RuntimeException{
    public UnsupportedTagException(String message) {
        super(message);
    }
}
