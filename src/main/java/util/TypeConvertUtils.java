package util;
import exception.ByteConverterException;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;


public class TypeConvertUtils {

    /**
     * @param bytes Byte array to read
     * @return unsigned int number (little-endian order)
     */
    public static long convertUInt32(byte bytes[]){
        return ByteBuffer.wrap(bytes).order(ByteOrder.LITTLE_ENDIAN).getInt() & 0xffffffffL;
    }

    /**
     * @param bytes Byte array to read
     * @return unsigned variable length number (little-endian order) mapped in BigInteger object
     */
    public static BigInteger convertVLN(byte[] bytes){
        byte[] bigEndianBytes = new byte[bytes.length];
        for (int i = 0; i<bytes.length; i++){
            bigEndianBytes[bytes.length-i-1] = bytes[i];
        }
        return new BigInteger(1, bigEndianBytes);
    }

    /**
     * @param bytes Byte array to read
     * @return floating point variable length number (little-endian order) mapped in BigDecimal object
     */
    public static BigDecimal convertFVLN(byte[] bytes){
        int digitsAfterPoint = (int)bytes[0];
        byte numberBytes[] = new byte[bytes.length-1];
        System.arraycopy(bytes, 1, numberBytes, 0, bytes.length-1);
        BigDecimal resultDecimal = new BigDecimal(convertVLN(numberBytes));
        resultDecimal = resultDecimal.movePointLeft(digitsAfterPoint);
        return resultDecimal;
    }

    public static String convertString(byte[] bytes){
        try {
            return new String(bytes, "CP866");
        } catch (UnsupportedEncodingException e) {
            throw new ByteConverterException("Can't convert byte array to string");
        }
    }
}
